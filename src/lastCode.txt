 public static void wingsON(Location loc, Location loc2, ParticleEffect pe)
  {
    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(1.25D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(1.05D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.855D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.65D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.45D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.25D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.25D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.45D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.65D)), 30.0D);
    loc.add(0.0D, 0.0D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(0.855D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(1.05D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(1.25D)), 30.0D);
    loc.add(0.0D, 0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-1.25D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-1.05D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.855D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.65D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.45D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.25D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.25D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.45D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.65D)), 30.0D);
    loc.add(0.0D, 0.0D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-0.855D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-1.05D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();

    ParticleEffect.FLAME.display(0.0F, 0.0F, 0.0F, 0.0F, 1, loc2.clone().add(loc2.getDirection().multiply(-1.25D)), 30.0D);
    loc.add(0.0D, -0.2D, 0.0D);
    loc2 = loc.clone();
  }



































    static int taskSantas;
    public static float countSantas1 = 0.0F;
    public static float countSantas2 = 0.0F;
    public static float countSantas3 = 0.0F;
    public static float countSantas4 = 0.0F;


    public static void santasCheck(Player p) {
        System.out.println("Name of snow: " + ParticlesG.guiName);
        taskSantas = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("ParticlesGUI"), new Runnable() {
            @Override
            public void run() {
                Location loc = p.getLocation();
                double x = Math.sin(0.99269908169872414D * countSantas1) * 1.5D;
                double y = 2.2D;
                double z = Math.cos(0.99269908169872414D * countSantas1) * 1.5D;
                Vector v = new Vector(0, y, 0);
                loc.add(v);
                countSantas1 += 1.0F;

                ParticleJapanes.sendParticles(EnumParticle.FLAME, loc, 2.0F, 1.0F, 2.0F, 0.05F, 50);
                /*
                ParticleEffect.ParticleColor color;
                color = new ParticleEffect.OrdinaryColor(0, 255, 0);
                ParticleEffect.REDSTONE.display(color, loc, 80.0D);

                Location loc2 = p.getLocation();
                double x2 = Math.sin(0.99269908169872414D * countSantas2) * 1.25D;
                double y2 = 0.65D;
                double z2 = Math.cos(0.99269908169872414D * countSantas2) * 1.25D;
                Vector v2 = new Vector(x2, y2, z2);
                loc2.add(v2);
                countSantas2 += 1.0F;

                color = new ParticleEffect.OrdinaryColor(254, 0, 0);
                ParticleEffect.REDSTONE.display(color, loc2, 80.0D);

                color = new ParticleEffect.OrdinaryColor(0, 255, 0);

                Location loc3 = p.getLocation();
                double x3 = Math.sin(0.99269908169872414D * countSantas3) * 0.75D;
                double y3 = 1.25D;
                double z3 = Math.cos(0.99269908169872414D * countSantas3) * 0.75D;
                Vector v3 = new Vector(x3, y3, z3);
                loc3.add(v3);
                countSantas3 += 1.0F;

                ParticleEffect.REDSTONE.display(color, loc3, 180.0D);

                Location loc4 = p.getLocation();
                double x4 = Math.sin(0.99269908169872414D * countSantas4) * 0.5D;
                double y4 = 2.025D;
                double z4 = Math.cos(0.99269908169872414D * countSantas4) * 0.5D;
                Vector v4 = new Vector(x4, y4, z4);
                loc4.add(v4);
                countSantas4 += 1.0F;

                ParticleEffect.REDSTONE.display(color, loc4,80.0D);

                color = new ParticleEffect.OrdinaryColor(255,255,255);
                Location locationStar = p.getLocation();
                double yStar = 2.2D;
                Vector vStar = new Vector(0, yStar, 0);
                locationStar.add(vStar);
                ParticleEffect.REDSTONE.display(color, locationStar,80.0D);
*/
            }
        }, 0L, 0L);

    }


    public static void santasStop(Player p)
    {
        Bukkit.getScheduler().cancelTask(Santas.taskSantas);
    }