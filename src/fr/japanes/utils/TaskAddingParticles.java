package fr.japanes.utils;

import fr.japanes.ParticlesG;
import fr.japanes.contains.ArrayListAll;
import fr.japanes.form.OnAroundCircle;
import fr.japanes.form.OnFootCircle;
import fr.japanes.form.OnHeadCircle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Gio on 23/12/2017.
 */
public class TaskAddingParticles {

    public static void onTaskAdding(int i, Player player){
        // I would like head particle and remove arround particle
        if(!ArrayListAll.ParticlesLocation.containsKey(player)){ArrayListAll.ParticlesLocation.put(player, i);}

        if(i== 1 && ArrayListAll.ParticlesLocation.get(player)==1){
            Bukkit.getScheduler().cancelTask(OnHeadCircle.taskHead);
        }

        if(i== 2 && ArrayListAll.ParticlesLocation.get(player)==2){
            Bukkit.getScheduler().cancelTask(OnAroundCircle.taskArround);
            OnAroundCircle.arroundhelix = 0.0F;
        }

        if(i== 3 && ArrayListAll.ParticlesLocation.get(player)==3){
            Bukkit.getScheduler().cancelTask(OnFootCircle.taskBellow);
        }

        if(i==1 && ArrayListAll.ParticlesLocation.get(player)==2){
            Bukkit.getScheduler().cancelTask(OnAroundCircle.taskArround);
        }

        if(i==1 && ArrayListAll.ParticlesLocation.get(player)==3){
            Bukkit.getScheduler().cancelTask(OnFootCircle.taskBellow);
        }

        // I would like head particle and remove arround particle
        if(i==2 && ArrayListAll.ParticlesLocation.get(player)==1){
            Bukkit.getScheduler().cancelTask(OnHeadCircle.taskHead);
        }

        if(i==2 && ArrayListAll.ParticlesLocation.get(player)==3){
            Bukkit.getScheduler().cancelTask(OnFootCircle.taskBellow);
        }

        // I would like head particle and remove arround particle
        if(i==3 && ArrayListAll.ParticlesLocation.get(player)==1){
            Bukkit.getScheduler().cancelTask(OnHeadCircle.taskHead);
        }

        if(i==3 && ArrayListAll.ParticlesLocation.get(player)==2){
            Bukkit.getScheduler().cancelTask(OnAroundCircle.taskArround);
        }
        ArrayListAll.ParticlesLocation.remove(player);
        ArrayListAll.ParticlesLocation.put(player, i);
        player.sendMessage(ParticlesG.pluginName + " " + ParticlesG.ParticlesLocationHeadArroundAndBellow);
    }



}
