package fr.japanes.utils;


import net.minecraft.server.v1_9_R2.EnumParticle;
import net.minecraft.server.v1_9_R2.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Created by Gio on 23/12/2017.
 */
public class ParticleJapanes {

    public static void sendParticles(EnumParticle enumParticle, Location location, float xOffset, float yOffset, float zOffset, float speed, int count){
        float x = (float)location.getX();
        float y = (float)location.getY();
        float z = (float)location.getZ();
        PacketPlayOutWorldParticles packetPlayOutWorldParticles = new PacketPlayOutWorldParticles(enumParticle, true, x, y, z, xOffset, yOffset, zOffset, speed, count, null);
        for(Player players : Bukkit.getOnlinePlayers()){
            ((CraftPlayer)players).getHandle().playerConnection.sendPacket(packetPlayOutWorldParticles);
        }
    }

}
