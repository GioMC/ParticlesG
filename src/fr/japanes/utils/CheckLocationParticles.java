package fr.japanes.utils;

import fr.japanes.ParticlesG;
import fr.japanes.contains.ArrayListAll;
import fr.japanes.form.OnAroundCircle;
import fr.japanes.form.OnFootCircle;
import fr.japanes.form.OnHeadCircle;
import net.minecraft.server.v1_9_R2.EnumParticle;
import org.bukkit.entity.Player;

/**
 * Created by Gio on 23/12/2017.
 */
public class CheckLocationParticles {

    public static void checkLocationParticles(EnumParticle enumParticle, Player player){
        if(!ArrayListAll.ParticlesLocation.containsKey(player)){
            player.sendMessage(ParticlesG.pluginName + " " + ParticlesG.ChooseLocationParticle);
            return;
        }
        int i = ArrayListAll.ParticlesLocation.get(player);
        TaskAddingParticles.onTaskAdding(i, player);
        if(i==1){
            OnHeadCircle.onHeadCircle(enumParticle, player);
        }
        if(i==2){
            OnAroundCircle.onArroundLocation(enumParticle, player);
        }
        if(i==3){
            OnFootCircle.onBellowCircle(enumParticle, player);
        }
        player.closeInventory();
        player.sendMessage(ParticlesG.pluginName + " " + ParticlesG.ParticlesActivated);
    }


}
