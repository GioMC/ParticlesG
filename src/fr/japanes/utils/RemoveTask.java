package fr.japanes.utils;

import fr.japanes.ParticlesG;
import fr.japanes.contains.ArrayListAll;
import fr.japanes.form.OnAroundCircle;
import fr.japanes.form.OnFootCircle;
import fr.japanes.form.OnHeadCircle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Gio on 24/12/2017.
 */
public class RemoveTask {

    public static void onRemoveTask(Player player){
        if(!ArrayListAll.ParticlesLocation.containsKey(player)){
            return;
        }
        int i = ArrayListAll.ParticlesLocation.get(player);
        ArrayListAll.ParticlesLocation.remove(player);
        if(i==1){Bukkit.getScheduler().cancelTask(OnHeadCircle.taskHead);}
        if(i==2){Bukkit.getScheduler().cancelTask(OnAroundCircle.taskArround);}
        if(i==3){Bukkit.getScheduler().cancelTask(OnFootCircle.taskBellow);}
        player.closeInventory();
        player.sendMessage(ParticlesG.pluginName + " " + ParticlesG.ParticlesDisabled);
    }


}
