package fr.japanes;


import fr.japanes.gui.ClickToGui;
import fr.japanes.gui.GuiParticles;
import fr.japanes.gui.OpenParticlesGuiCommand;
import fr.japanes.leaveP.LeavePlayer;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class ParticlesG
        extends JavaPlugin
{

  public static String pluginName = "";
  public static String guiName = "";

  public static String SnowParticleName = "";
  public static String FlameParticleName = "";
  public static String RedstoneParticleName = "";
  public static String HappyVillagerParticleName = "";
  public static String AngryVillagerParticleName = "";
  public static String NoteParticleName = "";
  public static String SmokeParticleName = "";
  public static String WaterParticleName = "";
  public static String LavaParticleName = "";
  public static String HeartParticleName = "";
  public static String SlimeParticleName = "";
  public static String CactusParticleName = "";

  public static String HeadParticleName = "";
  public static String AroundParticleName = "";
  public static String FootParticleName = "";
  public static String RemoveParticleName = "";

  public static String ChooseLocationParticle = "";
  public static String ParticlesActivated = "";
  public static String ParticlesLocationHeadArroundAndBellow = "";
  public static String ParticlesDisabled = "";


  public File file = new File(getDataFolder() + File.separator + "ParticlesG.yml");
  public FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);

  public void onEnable()
  {
    if(!getDataFolder().exists()){
      getDataFolder().mkdir();
    }
    /**
     * Get config gui and plugin name
     */
    /**
     * Get all name of particles
     */
    if(!file.exists()){
      try {
        file.createNewFile();
        setConfig();
        loadConfig();
      }catch (IOException e){e.printStackTrace();}
    }else{
      loadConfig();
    }


    System.out.println(pluginName + " §e has been enabled !");

    CommandExecutor particles = new OpenParticlesGuiCommand();
    getCommand("particles").setExecutor(particles);
    getServer().getPluginManager().registerEvents(new GuiParticles(), this);
    getServer().getPluginManager().registerEvents(new ClickToGui(), this);
    getServer().getPluginManager().registerEvents(new LeavePlayer(), this);
  }

  public void onDisable() {

  }

  public void setConfig(){
    fileConfiguration.set("PluginName", "ParticleGui");
    fileConfiguration.set("GuiName", "§7[§6§lParticlesG§7] §eAll Particles");
    fileConfiguration.set("SnowParticleName", "§fSnow");
    fileConfiguration.set("FlameParticleName", "§cFlame");
    fileConfiguration.set("RedstoneParticleName", "§4Redstone");
    fileConfiguration.set("HappyVillagerParticleName", "§aHappy villager");
    fileConfiguration.set("AngryVillagerParticleName", "§4Angry villager");
    fileConfiguration.set("NoteParticleName", "§9Note");
    fileConfiguration.set("SmokeParticleName", "§5Smoke");
    fileConfiguration.set("WaterParticleName", "§bWater");
    fileConfiguration.set("LavaParticleName", "§4Lava");
    fileConfiguration.set("HeartParticleName", "§2Heart");
   // fileConfiguration.set("SlimeParticleName", "§8Slime");
    //fileConfiguration.set("CactusParticleName", "§6Cactus");
    fileConfiguration.set("HeadParticleName", "§6Head");
    fileConfiguration.set("AroundParticleName", "§6Around");
    fileConfiguration.set("FootParticleName", "§6Foot");
    fileConfiguration.set("RemoveParticleName", "§cRemove particle");
    fileConfiguration.set("ChooseLocationParticle", "§cPlease chose location particles");
    fileConfiguration.set("ParticlesActivated", "§eYou have been actived your particles !");
    fileConfiguration.set("ParticlesDisabled", "§eYou have been remove your particles !");
    fileConfiguration.set("ParticlesLocationHeadArroundAndBellow", "§eYou have been choose the location particle, make sur select the particle !");

    try {
      fileConfiguration.save(file);
    }catch (IOException e){
      e.printStackTrace();
    }
  }

  public void loadConfig(){
    this.pluginName = fileConfiguration.get("PluginName").toString();
    this.guiName = fileConfiguration.get("GuiName").toString();
    this.SnowParticleName = fileConfiguration.get("SnowParticleName").toString();
    this.FlameParticleName = fileConfiguration.get("FlameParticleName").toString();
    this.RedstoneParticleName = fileConfiguration.get("RedstoneParticleName").toString();
    this.HappyVillagerParticleName = fileConfiguration.get("HappyVillagerParticleName").toString();
    this.AngryVillagerParticleName = fileConfiguration.get("AngryVillagerParticleName").toString();
    this.NoteParticleName = fileConfiguration.get("NoteParticleName").toString();
    this.SmokeParticleName = fileConfiguration.get("SmokeParticleName").toString();
    this.WaterParticleName = fileConfiguration.get("WaterParticleName").toString();
    this.LavaParticleName = fileConfiguration.get("LavaParticleName").toString();
    this.HeartParticleName = fileConfiguration.get("HeartParticleName").toString();
    //this.SlimeParticleName = fileConfiguration.get("SlimeParticleName").toString();
    //this.CactusParticleName = fileConfiguration.get("CactusParticleName").toString();

    this.HeadParticleName = fileConfiguration.get("HeadParticleName").toString();
    this.AroundParticleName = fileConfiguration.get("AroundParticleName").toString();
    this.FootParticleName = fileConfiguration.get("FootParticleName").toString();
    this.RemoveParticleName = fileConfiguration.get("RemoveParticleName").toString();
    this.ChooseLocationParticle = fileConfiguration.get("ChooseLocationParticle").toString();
    this.ParticlesActivated = fileConfiguration.get("ParticlesActivated").toString();
    this.ParticlesDisabled = fileConfiguration.get("ParticlesDisabled").toString();
    this.ParticlesLocationHeadArroundAndBellow = fileConfiguration.get("ParticlesLocationHeadArroundAndBellow").toString();
  }




}
