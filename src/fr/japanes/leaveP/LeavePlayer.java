package fr.japanes.leaveP;

import fr.japanes.utils.RemoveTask;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeavePlayer
  implements Listener
{
  @EventHandler
  public void leavePlayer(PlayerQuitEvent e)
  {
    Player p = e.getPlayer();
    RemoveTask.onRemoveTask(p);
  }
  
  @EventHandler
  public void kickPlayer(PlayerKickEvent e)
  {
    Player p = e.getPlayer();
    RemoveTask.onRemoveTask(p);
  }
}
