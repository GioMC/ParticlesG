package fr.japanes.gui;

import fr.japanes.utils.CheckLocationParticles;
import fr.japanes.utils.RemoveTask;
import fr.japanes.utils.TaskAddingParticles;
import net.minecraft.server.v1_9_R2.EnumParticle;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ClickToGui
  implements Listener
{
  @EventHandler
  public void onClick(InventoryClickEvent e)
  {
    Player p = (Player)e.getWhoClicked();
    if ((e.getCurrentItem() == null) || (e.getCurrentItem().getType() == Material.AIR) || (!e.getCurrentItem().hasItemMeta())) {
      return;
    }

    if(e.getCurrentItem().getType() == Material.IRON_HELMET){
      TaskAddingParticles.onTaskAdding(1, p);
    }

    if(e.getCurrentItem().getType() == Material.IRON_CHESTPLATE){
      TaskAddingParticles.onTaskAdding(2, p);
    }

    if(e.getCurrentItem().getType() == Material.IRON_BOOTS){
      TaskAddingParticles.onTaskAdding(3, p);
    }

    if (e.getCurrentItem().getType() == Material.TORCH) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.FLAME, p);
    }

    if (e.getCurrentItem().getType() == Material.REDSTONE) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.REDSTONE, p);
    }

    if (e.getCurrentItem().getType() == Material.SNOW_BALL) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.SNOW_SHOVEL, p);
    }

    if (e.getCurrentItem().getType() == Material.EMERALD) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.VILLAGER_HAPPY, p);
    }

    if (e.getCurrentItem().getType() == Material.JUKEBOX) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.NOTE, p);
    }

    if (e.getCurrentItem().getType() == Material.FLINT_AND_STEEL) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.SMOKE_NORMAL, p);
    }

    if (e.getCurrentItem().getType() == Material.WATER_BUCKET) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.WATER_SPLASH, p);
    }

    if (e.getCurrentItem().getType() == Material.LAVA_BUCKET) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.DRIP_LAVA, p);
    }

    if (e.getCurrentItem().getType() == Material.RED_ROSE) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.HEART, p);
    }

    if (e.getCurrentItem().getType() == Material.SULPHUR) {
      CheckLocationParticles.checkLocationParticles(EnumParticle.VILLAGER_ANGRY, p);
    }

    if(e.getCurrentItem().getType() == Material.BARRIER){
      RemoveTask.onRemoveTask(p);
    }

    e.setCancelled(true);
  }




  
}
