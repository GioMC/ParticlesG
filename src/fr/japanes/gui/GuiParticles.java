package fr.japanes.gui;


import fr.japanes.ParticlesG;
import fr.japanes.utils.CreateItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;

public class GuiParticles
  implements Listener
{




  public  static void opengui(Player p)
  {

    Inventory inv = Bukkit.createInventory(null, 36, ParticlesG.guiName);
    
    inv.setItem(1, CreateItem.createItem(Material.TORCH, ParticlesG.FlameParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(2, CreateItem.createItem(Material.REDSTONE, ParticlesG.RedstoneParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(3, CreateItem.createItem(Material.SNOW_BALL, ParticlesG.SnowParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(4, CreateItem.createItem(Material.EMERALD, ParticlesG.HappyVillagerParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(5, CreateItem.createItem(Material.JUKEBOX, ParticlesG.NoteParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(6, CreateItem.createItem(Material.FLINT_AND_STEEL, ParticlesG.SmokeParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(7, CreateItem.createItem(Material.WATER_BUCKET, ParticlesG.WaterParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(12, CreateItem.createItem(Material.LAVA_BUCKET, ParticlesG.LavaParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(13, CreateItem.createItem(Material.RED_ROSE, ParticlesG.HeartParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(14, CreateItem.createItem(Material.SULPHUR, ParticlesG.AngryVillagerParticleName, "§a> §fLeft click to display this particle"));
    
    inv.setItem(27, CreateItem.createItem(Material.IRON_HELMET, ParticlesG.HeadParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(28, CreateItem.createItem(Material.IRON_CHESTPLATE, ParticlesG.AroundParticleName, "§a> §fLeft click to display this particle"));
    inv.setItem(29, CreateItem.createItem(Material.IRON_BOOTS,  ParticlesG.FootParticleName, "§a> §fLeft click to display this particle"));
    
    inv.setItem(35, CreateItem.createItem(Material.BARRIER, ParticlesG.RemoveParticleName, "§a> §fLeft click to display this particle"));
    
    p.openInventory(inv);
  }
}
