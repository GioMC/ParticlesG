package fr.japanes.form;

import fr.japanes.utils.ParticleJapanes;
import net.minecraft.server.v1_9_R2.EnumParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * Created by Gio on 23/12/2017.
 */
public class OnFootCircle {

    public static int taskBellow;

    public static void onBellowCircle(EnumParticle enumParticle, Player p)
    {

        taskBellow = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("ParticlesG"), new Runnable()
        {
            float count = 0.0F;

            public void run()
            {
                Location loc = p.getLocation();

                double x = Math.sin(0.99269908169872414D * count) * 0.5D;
                double y = 0.1D;
                double z = Math.cos(0.99269908169872414D * count) * 0.5D;
                Vector v = new Vector(x, y, z);
                loc.add(v);
                ParticleJapanes.sendParticles(enumParticle, loc, 0.0F, 0.0F, 0.0F, 0, 80);
                count += 1.0F;
            }
        }, 0L, 0L);
    }


}
