package fr.japanes.form;

import fr.japanes.utils.ParticleJapanes;
import net.minecraft.server.v1_9_R2.EnumParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * Created by Gio on 23/12/2017.
 */
public class OnAroundCircle {

    public static int taskArround;
    public static float arroundhelix = 0.0F;
    public static void onArroundLocation(EnumParticle enumParticle, Player p)
    {

        taskArround = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("ParticlesG"), new Runnable()
        {
            float count = 0.0F;

            public void run()
            {
                Location loc = p.getLocation();
                double x = Math.sin(-0.39269908169872414D * count) * -0.5D;
                double y = 0.01D * arroundhelix;
                double z = Math.cos(-0.39269908169872414D * count) *-0.5D;
                Vector v = new Vector(x, y, z);
                loc.add(v);
                count += 1.0F;
                arroundhelix += 1.0F;
                if (arroundhelix >= 200.1D) {
                    arroundhelix = 0.0F;
                }
                ParticleJapanes.sendParticles(enumParticle, loc, 0.0F, 0.0F, 0.0F, 0, 80);
            }
        }, 0L, 0L);
    }
}
